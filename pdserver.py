"""
Pd server class to send/receive essages to/from Pd using gobject.io_add_watch
for the server, thus running in a GTK loop.
Credits: socket documentation, gobject documentation (and pygtk FAQ)
This was also very useful: http://rox.sourceforge.net/desktop/node/413.html
for the listener and handler funcions.
"""

import gobject
import socket

class PdServer:
    def __init__(self, serverport=4321, clientport=4322):
        """ Server and start listening. Create client"""
        # SERVER
        self.serverhost = ''
        self.serverport = serverport
        self.sockserver = socket.socket()
        self.sockserver.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sockserver.bind((self.serverhost, self.serverport))
        self.sockserver.listen(1)
        self.inmessage = ''
        print "Listening..."
        gobject.io_add_watch(self.sockserver, gobject.IO_IN, self.listener)
        print "Added io_add_watch for socket"
        # CLIENT
        self.clienthost = ''
        self.clientport = clientport
        self.sockclient = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.outmessage = ''
        self.connect_client()

    def connect_client(self):
        """ Connect as client """
        try:
            self.sockclient.connect((self.clienthost, self.clientport))
            self.client_connected = True
            print "Connected to server on port %d" % self.clientport
        except:
            self.client_connected = False
            print "no server (yet?)..."

    def close_client(self):
        """ Close the client connection """
        pass

    def listener(self, sock, condition):
        """ Asynchronous connection listener.
        Starts a handler for each connection."""
        self.conn, self.addr = self.sockserver.accept()
        print "Connection to Server from:", self.addr
        gobject.io_add_watch(self.conn, gobject.IO_IN, self.handler)
        return True
 
    def handler(self, conn, condition):
        """ Asynchronous connection handler.
        Processes each line from the socket. """
        data = self.conn.recv(4096)
        if not len(data):
            print "Connection closed - " + str(self.addr)
            return False
        else:
            try:
                value = data[0:data.find(';')]
                value = value.split(' ')[1]
                self.inmessage = self.inmessage + data 
            except:
                print "Bad value in %s" % data
            #print ("Received: %s") % (data)
            return True

    def flush_inmessage(self):
        """ Flushes the inmessage received by the server"""
        return_mess = self.inmessage
        self.inmessage = ''
        return(return_mess)

    def out_queue(self, message):
        """ Queue a message to be sent as client to Pd """
        self.outmessage = self.outmessage + message

    def send_message(self):
        """ Send the message in self.outmessage """
        try:
            self.sockclient.sendall(self.outmessage)
            self.outmessage = ''
            return 0
        except:
            print "Error sending message. Messages still in queue"
            return 1




