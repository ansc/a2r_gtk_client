#!/usr/bin/env python
"""
Simple example of sending a counter via TCP to Pure Data on localhost. In PD the
[netreceive] object must use the same port specified by TCP_PORT. Python sockets
part based on Python tcp documentation.
"""
import socket
import time
import sys

class PdSender:
    """ Sends stuff to Pd, using the sock which must already be open etc. """
    def __init__(self):
        self.sock = None
        self.debug = False

    def debug_print(self,string):
        """ simple debug screen printer """
        if self.debug:
            print (string)

    def send(self,string):
        """ Send the message to self.sock """
        #check that the message has final ";" otherwise add it
        if string[-1] != ";":
            string = string + ";"
            self.debug_print(("Sending %s") % (string))
        try:
            self.sock.send(string)
            self.debug_print("OK")
            return 0
        except:
            print "Error sending..."
            return 1

class SockConnector:
    """ Create a socket and connect it to Pd, return the """
    def __init__(self, address="127.0.0.1", port=9876):
        """ Intitiate address and port """
        self.address = address
        self.port = port
        self.last_error = ""
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect(self):
        """ Connect """
        print (
                ("SockConnector: Connect to %s on port %d") %
                (self.address, self.port)
            )
        try:
            self.sock.connect((self.address, self.port))
            self.last_error = ""
            return 0
        except socket.error as err:
            print ("Error connecting: %s") % (err)
            self.last_error = ("Could not connect. %s") % (err)
            return err.errno

    def close(self):
        """ Close the connection """
        try:
            self.sock.close()
        except:
            pass

def _tester():
    """ tester """
    c = SockConnector()
    result = c.connect()
    if result != 0:
        print c.last_error
        sys.exit(result)
    ps = PdSender()
    ps.debug = True
    ps.sock = c.sock
    for x in range(1,61):   
        ps.send("Foo %d" % x)
        ps.send("Bar %d" % x**2)
        time.sleep(0.5)
    c.close()

# TEST #
#_tester()
