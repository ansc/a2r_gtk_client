#!/usr/bin/env python
""" Gtk client for Pd patch """
import sys
from collections import defaultdict
try:
    import pygtk
    pygtk.require('2.0')
    import gtk
except ImportError:
    print "You need pygtk 2.0 and gtk installed for this to work!"
    sys.exit(1)

import connector

class MainWindow:
    """ Class for the main window """
    def delete_event(self, widget, event, data=None):
        """ Close window and quit. """
        self.cancel = True
        gtk.main_quit()
        self.connector.close()
#        sys.exit(0)
        return False

    def connect_callback(self, widget):
        """ Create a socket, try to connect to Pd and return that socket """
        address = self.address_ernty.get_text()
        port = int(self.port_entry.get_text())
        self.connector = connector.SockConnector(address, port)
        result = self.connector.connect()
        if result != 0:
                err_diag = gtk.MessageDialog(
                        self.main_window,
                        0,
                        gtk.MESSAGE_ERROR, 
                        gtk.BUTTONS_OK, 
                        "Connection to %s:%s\n" % (address, port) +
                        self.connector.last_error)
                err_diag.run()
                err_diag.destroy()
        else:
            self.is_connected = True

    def send_callback(self, widget):
        """ Send the message to Pd """
        if not self.is_connected:
            err_diag = gtk.MessageDialog(
                    self.main_window,
                    0,
                    gtk.MESSAGE_ERROR, 
                    gtk.BUTTONS_OK, 
                    "Please connect first!"
                    )
            err_diag.run()
            err_diag.destroy()
            return 1
        self.ps = connector.PdSender()
        self.ps.debug = True
        self.ps.sock = self.connector.sock
        self.ps.send(self.message_ernty.get_text())

        #TODO

    def __init__(self, initial_file=''):
        """ Init function for main window """
        self.is_connected = False
        self.main_window = gtk.Window()
        self.main_window.set_size_request(700, 500)
        self.main_window.connect("delete_event", self.delete_event)
        self.main_window.set_position(gtk.WIN_POS_CENTER)

        self.address_box = gtk.HBox()
        self.address_ernty = gtk.Entry()
        self.address_ernty.set_text('127.0.0.1')
        self.port_entry = gtk.Entry()
        self.port_entry.set_text('6060')
        self.connect_button = gtk.Button('connect')
        self.connect_button.connect('clicked',self.connect_callback)
        self.address_box.pack_start(self.address_ernty, True, True)
        self.address_box.pack_start(self.port_entry, False, False)
        self.address_box.pack_start(self.connect_button, False, False)

        self.message_box = gtk.HBox()
        self.message_ernty = gtk.Entry()
        self.message_ernty.set_text('freq 220')
        self.send_button = gtk.Button('send')
        self.send_button.connect('clicked',self.send_callback)
        self.message_box.pack_start(self.message_ernty, True, True)
        self.message_box.pack_start(self.send_button, False, True)
        
        self.main_vertical = gtk.VBox()
        self.main_vertical.pack_start(self.address_box, False, True)
        self.main_vertical.pack_start(self.message_box, False, True)

        self.main_window.add(self.main_vertical)
        print "Gtk Pd client"
        self.main_window.show_all()     

def main():
    """ main function """
    gtk.main()   

if __name__ == "__main__":
    main_win = MainWindow()
    main()
